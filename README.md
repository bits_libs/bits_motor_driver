# **DC-Motor Driver**

Eine Klasse zur Ansteuerung des Gleichstrommotors von BIT<sup>S-i</sup>. 
Im Gegensatz zur `thk_MotorDriverTB6612FNG` Klasse werden hier die Pins für den Motortreiber direkt mitübergeben.

Getestet mit:

- Arduino Mega Pro + TB6612FNG<br />
<br />

## **Voraussetzung:**

- [thk_motor_driver](https://git-ce.rwth-aachen.de/thk_libs/microcontrollers/thk-motor-driver.git)<br />
<br />

## **Installation:**

Um diese Klassen verwenden zu können, muss dieses Repository geklont und in das Library-Verzeichnis der Arduino-IDE kopiert werden.<br />
<br />

## **Anwendung:**

Zur Verwendung siehe zunächst das Beispiel `drive.ino`<br />
<br />

**Erläuterung zur Klasse**

Es wird für jeden Motortreiber nur ein Objekt benötigt, dem beim instanziieren alle benötigten Informationen übergeben wird.<br />
<br />

**Einbinden der Libraries:**

```arduino
#include <bits_motor_driver_tb6612fng.h>
```

**Instanziieren eines Motortreiber-Objektes:**

```arduino
bits_MotorDriverTB6612FNG motorExample
```

**Funktionen:**

```arduino
motorExample.drive(velocity, direction);    // Fahren mit definierter Geschwindigkeit (0-255) und mit Angabe der Richtung. 1 = Vorwärts, 0 = Rückwärts
motorExample.drive_forward(velocity);       // Vorwärtsfahren mit definierter Geschwindigkeit (0-255)
motorExample.drive_backward(velocity);      // Rückwärtsfahren mit definierter Geschwindigkeit (0-255)
motorExample.stop();                        // Stoppen des Motors
```