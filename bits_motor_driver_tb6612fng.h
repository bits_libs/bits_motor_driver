/*
 * Klasse: bits_MotorDriverTB6612FNG
 *
 * Zur Ansteuerung des Gleichstrommotors vom BITS-Car
 * Im Vergleich zur thk_MotorDriverTB6612FNG Klasse werden hier die
 * Pins direkt mitübergeben.
 * 
 */

#ifndef BITSMOTORDRIVERTB6612FNG_H
#define BITSMOTORDRIVERTB6612FNG_H

#include <Arduino.h>
#include "thk_motor_driver_tb6612fng.h"

class bits_MotorDriverTB6612FNG : public thk_MotorDriverTB6612FNG
{
    public:
        bits_MotorDriverTB6612FNG() : thk_MotorDriverTB6612FNG(1, 38, 6, 40, 42, 0, 0, 0){};

    private:

};

#endif
