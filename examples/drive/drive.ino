#include <bits_motor_driver_tb6612fng.h>

uint8_t velocity = 255;

bits_MotorDriverTB6612FNG motor;

void setup()
{
    Serial.begin(115200);
    motor.init();
}

void loop()
{
    motor.drive_forward(velocity);
    delay(2000);

    motor.stop();
    delay(1000);

    motor.drive_backward(velocity);
    delay(2000);
    
    motor.stop();
    delay(1000);

    motor.drive(velocity, 1);
    delay(2000);

    motor.stop();
    delay(1000);

    motor.drive(velocity, 0);
    delay(2000);
    
    motor.stop();
    delay(1000);
}